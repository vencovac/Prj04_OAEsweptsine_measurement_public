function mainBoxModel_SteadyStateTonesFCE(gain,L1,L2,f2f1,rough,RScale)
% function mainBoxModel_SteadyStateTonesFCE(gain,L1,L2,f2f1,rough,RScale)
% the function runs box model for discrete tone inputs (two tones to generate DPOAEs)
% frequency range of the tones is determined in the function
% (RUNNING IS VERY LONG!!!)
% input parameters:
% gain - model gain (1.05 used in the paper)
% L1 - Level of f1 tone (dB SPL)
% L2 - level of f2 tone (dB SPL)
% f2f1 - freq ratio f2/f1
% rough - with inhomogen or not? set 21 for smooth cochlea or 19 for random
% roughness
% RScale - standard deviation of the inhomogeneities (0.05 was used in the
% paper)
% 
% Author: Vaclav Vencovsky
% Department of Radioelectronics, CTU in Prague
% email address: vaclav.vencovsky@gmail.com
% Website: https://mmtg.fel.cvut.cz/personal/vencovsky/
% July 2021; Last revision: 21-Jul-2021


%% addpaths
addpath('fce_boxmodel/')

%% -- input tones
fs = 600e3; % sampling rate
Nsamp = 60000; % 100 ms for 10 Hz resolution
F1vect = (100:1000)*fs/Nsamp; % F1 vect between 1kHz and 10 kHz
F2vect = f2f1*F1vect;
Fdpvect = 2*F1vect - F2vect;
 
idxUsed = (~mod(Fdpvect,10)); % find indexes assurin no spectral leaking

F1vect = F1vect(idxUsed);
F2vect = F2vect(idxUsed);
Fdpvect = Fdpvect(idxUsed);

% finding the optimal number (mimimal number) of samples needed for
% constructin fft without spectral leakage for every combination of F1vect,
% F2vect and Fdpvect
NsampOpt = zeros(size(F1vect));

for fv=1:length(F1vect)
    i1 = 1;
    i2 = 1;
    idp = 1;
    Nsampmin1 = [];
    Nsampmin2 = [];
    Nsampmindp = [];
    
    for k=1:10000
        Nsampmin = k*fs./F1vect(fv);
        if Nsampmin == round(Nsampmin)
            Nsampmin1(i1) = Nsampmin;
            i1 = i1 + 1;
        end
        Nsampmin = k*fs./F2vect(fv);
        if Nsampmin == round(Nsampmin)
            Nsampmin2(i2) = Nsampmin;
            i2 = i2 + 1;
        end
        Nsampmin = k*fs./Fdpvect(fv);
        if Nsampmin == round(Nsampmin)
            Nsampmindp(idp) = Nsampmin;
            idp = idp + 1;
        end
    end
    NsampOpt(fv) = min(intersect(intersect(Nsampmin1,Nsampmin2),Nsampmindp)); % finding the smallest common value
    
    
end


%% -- box model parameters
FolderRes = 'Simulations/DPss/';
% gain = 1.05;
% rough = 21; % no roughness
% RScale = 0; % 0 std in roughness
Ma = 2e-1; % for visualization, scale of the y axis in the BM reponse

Tonset = 0.03; % onset time (to achieve steady state response)
% L1 = 50; % level of the f1 tone dB SPL
% L2 = 50; % level of the f2 tone dB SPL
phi1 = 0;
phi2 = 0;
T2on = 0;

visual = 1;
Nonset = floor(Tonset*fs); % number of samples in the onset 


for k=1:length(F1vect)
    F1 = F1vect(k);
    F2 = F2vect(k);
    Fdp = Fdpvect(k);
    Nwin = NsampOpt(k);
    
    fx = (0:Nwin-1)*fs/Nwin; % frequency axis

    idxFdp = find(fx==Fdp);
    idxF1 = find(fx==F1);
    idxF2 = find(fx==F2);

    
    
    Tdur = Tonset+Nwin/fs;
    T2off = Tdur+0.02; % to achieve steady state response until the end of the simulation (no offset ramping, it is not needed)
    T1off = Tdur+0.02;
    
    
    [oae,tim,x,RR,UU,UUr,oaeSt,TMdisp]=CochleaRGB_DP_DFT_MEtalNL(L1,F1,L2,F2,Tdur,Ma,gain,phi1,phi2,T2on,T2off,T1off,visual,rough,RScale);
    tvect{k} = tim;
    UUrdpF = fft(UUr(:,Nonset:Nwin+Nonset-1).')/Nwin;
    UUrdp{k} = 2*UUrdpF(idxFdp,:).*exp(-sqrt(-1)*2*pi*Fdp*Nonset*1/fs);
    UUdpF = fft(UU(:,Nonset:Nwin+Nonset-1).')/Nwin;
    UUdp{k} = 2*UUdpF(idxFdp,:).*exp(-sqrt(-1)*2*pi*Fdp*Nonset*1/fs);
    oaeF = fft(oae(Nonset:Nwin+Nonset-1))/Nwin.*exp(-sqrt(-1)*2*pi*Fdp*Nonset*1/fs);
    oaedp(k) = 2*oaeF(idxFdp);
    oaeFSt = fft(oaeSt(Nonset:Nwin+Nonset-1))/Nwin.*exp(-sqrt(-1)*2*pi*Fdp*Nonset*1/fs);
    oaedpSt(k) = 2*oaeFSt(idxFdp);
        
    RRF = fft(RR(:,Nonset:Nwin+Nonset-1).')/Nwin;
    RRdp{k} = 2*RRF(idxFdp,:).*exp(-sqrt(-1)*2*pi*Fdp*Nonset*1/fs);
        
    RRF1{k} = 2*RRF(idxF1,:).*exp(-sqrt(-1)*2*pi*F1*Nonset*1/fs);
    RRF2{k} = 2*RRF(idxF2,:).*exp(-sqrt(-1)*2*pi*F2*Nonset*1/fs);
end

save([FolderRes 'DP_L1_' num2str(L1) 'dB_L2_' num2str(L2) 'dB_f2f1' num2str(f2f1) 'gain' num2str(gain*100) 'R' num2str(rough) 'rs_' num2str(100*RScale) '.mat'],...
    'UUrdp','oaedp','oaedpSt','x','RRdp','RRF1','RRF2','UUdp','L1','L2','F1vect','F2vect','Fdpvect','gain','rough','RScale','tvect','NsampOpt');

      %% rmpath
rmpath('fce_boxmodel/')
