
% example how to run boxmodel for two tone input getting steady state
% DPOAEs

gain = 1.05; % parameter affecting gain in the model
L1 = 60; % L1 level in dB
L2 = 50;  % L2 level in dB
f2f1 = 1.2; % f2/f1 ratio
rough = 19; % 19 yields a model with roughness, 21 yields model without roughness
Rscale = 0.05; % value which sets an extent of roughness


mainBoxModel_SteadyStateTonesFCE(gain,L1,L2,f2f1,rough,Rscale);

