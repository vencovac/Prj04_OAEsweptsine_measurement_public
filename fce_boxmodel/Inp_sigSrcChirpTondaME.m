function [Sig]=Inp_sigSrcChirpTondaME(F2s,F2e,A1,A2,F2F1,t,phi1,phi2,Td)

% 
%
% S is a parameter which states which signal is generated
% S = 0   - F1 and F2 tones turned on simultaneously, ramped durHWF1,
% S = 2   - F1 + F2 pulsed, and also with supressor
% other S - F1 + F2 pulsed

  AM1=db2inputME(A1);%*om1^2;   %second time derivative
  AM2=db2inputME(A2);%*om2^2;   %second time derivative
  %omdp=2*pi*(2*F1-F2);
  durHW = 10e-3; % duration of ramp
  %durHWF1 = 4e-3; % duration of F1 ramp
  
  F1s = F2s/F2F1;
  F1e = F2e/F2F1;
  
  %durHW = 15e-3; % duration of ramp
  durHWF1 = 10e-3; % duration of F1 ramp
  
  
  %T2on = 0.02; % onset of F2 tone
  %T2off = 0.12; % offset of F2 tone
  %T1off = 0.14; % offset of F1 tone
%   
%   T2on = 0.01; % onset of F2 tone
%   T2off = 0.04; % offset of F2 tone
%   T1off = 0.05; % offset of F1 tone
  
%  if S==0   
%     
%      Sig=m_hann(t,durHW)*((AM1.*cos(om1*t+phi1)+AM2.*cos(om2*t+phi2)));
%      AM=max(abs(Sig));  
%  elseif S==2 % suppressor
%      %Sig=m_hann(t,durHW)*(AM1.*cos(om1*t+phi1));
%      
%      Sig=0;
    L2 = Td/log(F2e/F2s);
    L1 = Td/log(F1e/F1s);

      if t<durHWF1  
          Sig = m_hann(t,durHWF1)*AM1*sin(2*pi*F1s*L1*(exp(t/L1))+phi1) + m_hann(t,durHWF1)*AM2*sin(2*pi*F2s*L2*(exp(t/L2))+phi2);
       elseif (t>=durHWF1)&&(t<Td-durHWF1)
          Sig = AM1*sin(2*pi*F1s*L1*(exp(t/L1))+phi1) + AM2*sin(2*pi*F2s*L2*(exp(t/L2))+phi2);
       elseif (t>=Td-durHWF1)&&(t<=Td)    
          Sig = m_hann(Td-t,durHWF1)*AM1*sin(2*pi*F1s*L1*(exp(t/L1))+phi1) + m_hann(Td-t,durHWF1)*AM2*sin(2*pi*F2s*L2*(exp(t/L2))+phi2);
      end
%            Sig=m_hann(t,durHWF1)*AM1*cos(om1*t+phi1)+...
%                m_hann(T2off-t,durHW)*AM2*cos(om2*t+phi2)+m_hann(t,durHWF1)*AMsup*cos(0.9*omdp*t+phi1);
%       elseif (t>=T2off)&&(t<T1off-durHWF1)
%           Sig=AM1*cos(om1*t+phi1)+AMsup*cos(0.9*omdp*t+phi1);
%       elseif (t>=T1off-durHWF1)&(t<T1off)
%           Sig=m_hann(T1off-t,durHWF1)*AM1*cos(om1*t+phi1)+m_hann(T1off-t,durHWF1)*AMsup*cos(0.9*omdp*t+phi1);
%       else
%           Sig=0;
%       end
%      
%  else  
%      Sig=0;
%       if t<T2on  
%           Sig=m_hann(t,durHWF1)*AM1*cos(om1*t+phi1);
%        elseif (t>=T2on)&&(t<T2off-durHW)
%           Sig=m_hann(t,durHWF1)*AM1*cos(om1*t+phi1)+...
%               m_hann(t-T2on,durHW)*AM2*cos(om2*t+phi2);
%        elseif (t>=T2off-durHW)&&(t<T2off)    
%            Sig=m_hann(t,durHWF1)*AM1*cos(om1*t+phi1)+...
%                m_hann(T2off-t,durHW)*AM2*cos(om2*t+phi2);
%       elseif (t>=T2off)&&(t<T1off-durHWF1)
%           Sig=AM1*cos(om1*t+phi1);
%       elseif (t>=T1off-durHWF1)&(t<T1off)
%           Sig=m_hann(T1off-t,durHWF1)*AM1*cos(om1*t+phi1);
%       else
%           Sig=0;
%       end
%      %

%      
%      A = 1;
% 
%     
%     Sig = A*sin(2*pi*(F1s+(F1e-F1s)/(2*(Td))*t)*t+phi1) + A*sin(2*pi*(F2s+(F2e-F2s)/(2*(Td))*t)*t+phi2);


 
  function w=m_hann(t,dur)
      framp = 1/(2*dur);
      if t<=dur
        w = 0.5 * (1 - cos(2*pi*t*framp));  
      else
          w = 1;
      end