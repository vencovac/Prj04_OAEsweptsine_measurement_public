function [oae,tim,x,oaeSt]=CochleaRGB_DP_DFT_MEtalSweep(F2ob,F2oe,F2F1o,A2o,A1o,octperseco,gain,phi2o,phi1o,Rduro,visual,rough,roughScale)
% nonlinear force

if nargin < 1, F2ob = 1000; end
if nargin < 2, F2oe=4000; end
if nargin < 3, F2F1o=1.2; end
if nargin < 4,  A2o=50; end
if nargin < 5,  A1o=50; end
if nargin < 6,  octperseco=0.5;end
if nargin < 7,  gain=1.05; end
if nargin < 8,  phi2o=0; end
if nargin < 9,  phi1o=0; end
if nargin < 10, Rduro = 0.05; end
if nargin < 11, visual = 1; end
if nargin < 12, rough = 1; end
if nargin < 13, roughScale = 0.05; end


global Ginv DampSp  stiff bigamma wm2 undamp N MEinv kME hME Gme Sty gammaAir ...
      Ve GammaMi Gs Qbm Sow Wbm Pea
  
global f2b f2e f2f1 octpersec Lspl2 Lspl1 phi2s phi1s Rdur

ResetAll; %reset global


Lspl1=A1o;Lspl2=A2o; f2b=F2ob;f2e=F2oe; phi1s=phi1o; phi2s=phi2o; 
f2f1 = F2F1o; octpersec = octperseco; Rdur = Rduro;

h=5e-6/3;   %time step
fs=1/h;   %sampling frequency
I=sqrt(-1);

% swept sine parameters

Noct = log2(f2e/f2b); % give number of octaves
Tdur = Noct/octpersec;
%Nsamp = round(fsamp*T);
Ma = 2e-2;

   

%________________________MODEL PARAMETERS_____________________________________%
 N=800;
 rebuild_flag=1;
 
 [x,Ginv,stiff,DampSp,undamp,bigamma,wm2,Qbm,Qs, MEinv, kME, hME, Gme, Sty, gammaAir, ...
      Ve, GammaMi, Gs, Sow, Wbm, Pea] = alldataRGmePa(N,gain);
   
  %roughScale = 0.07;

 %randn(800,1)
 switch rough
     case 1 % constructive interfer
       load randstiff.mat;
       rv = 0*rv;
       gauss1 = gausswin(31,10);
       gaussA = [gauss1; zeros((800-31),1)];
       gaussA1 = circshift(gaussA,350-16,1);
       gaussA2 = circshift(gaussA,360-16,1);
       gaussA3 = circshift(gaussA,371-16,1);
       rv = gaussA1 + gaussA2 + gaussA3;
       roughScale = 0.11;
       
     case 2 % destructive interfer.
        load randstiff02.mat;
        rv = 0*rv;
       	gauss1 = gausswin(31,10);
        gaussA = [gauss1; zeros(800-31,1)];
        gaussA1 = circshift(gaussA,355-16,1);
        gaussA2 = circshift(gaussA,365-16,1);
        gaussA3 = circshift(gaussA,376-16,1);
        rv = gaussA1 + gaussA2 + gaussA3;
        roughScale = 0.11;
     case 3
        load randstiff03.mat
        TWnon = [tukeywin(41,0.3); zeros((800-41),1)];
        TWnsh = circshift(TWnon,341,1);
        rv = TWnsh.*rv;
        roughScale = 0.05;
        
     case 4
          load randstiff.mat;
       rv = 0*rv;
       gauss1 = gausswin(31,10);
       gaussA = [gauss1; zeros((800-31),1)];
       gaussA1 = circshift(gaussA,350-16-16,1);
       gaussA2 = circshift(gaussA,360-16-16,1);
       gaussA3 = circshift(gaussA,371-16-16,1);
       rv = gaussA1 + gaussA2 + gaussA3;
       roughScale = 0.11;
     case 5
        load randstiff03.mat
        TWnon = [tukeywin(48,0.3); zeros((800-48),1)];
        TWnsh = circshift(TWnon,208,1);
        rv = TWnsh.*rv;
        roughScale = 0.05;
     case 6
        load randstiff06.mat
        TWnon = [tukeywin(41,0.3); zeros((800-41),1)];
        TWnsh = circshift(TWnon,341,1);
        rv = TWnsh.*rv;
        roughScale = 0.05;
     case 7
       load randstiff07.mat
        TWnon = [tukeywin(41,0.3); zeros((800-41),1)];
        TWnsh = circshift(TWnon,341,1);
        rv = TWnsh.*rv;
        roughScale = 0.05;
     case 8
       load randstiff08.mat
        TWnon = [tukeywin(41,0.3); zeros((800-41),1)];
        TWnsh = circshift(TWnon,341,1);
        rv = TWnsh.*rv;
        roughScale = 0.05;
     case 9
       load randstiff09.mat
        TWnon = [tukeywin(41,0.3); zeros((800-41),1)];
        TWnsh = circshift(TWnon,341,1);
        rv = TWnsh.*rv;
        roughScale = 0.05;
     case 10
       load randstiff10.mat
        TWnon = [tukeywin(41,0.3); zeros((800-41),1)];
        TWnsh = circshift(TWnon,341,1);
        rv = TWnsh.*rv;
        roughScale = 0.05;
     case 11
       load randstiff11.mat;
       TWnon = [tukeywin(120,0.3); zeros((800-120),1)];
       TWnsh = circshift(TWnon,280,1);
       rv = TWnsh.*rv;
       roughScale = 0.05;
     case 12
       load randstiff12.mat;
       rv = 0*rv;
       gauss1 = gausswin(31,10);
       gaussA = [gauss1; zeros((800-31),1)];
       %gaussA1 = circshift(gaussA,350-16-16,1);
       gaussA2 = circshift(gaussA,360-16-16,1);
       %gaussA3 = circshift(gaussA,371-16-16,1);
       rv = gaussA2;
       roughScale = 0.11;
     case 13
       load randstiff13.mat
           TWnon = [tukeywin(120,0.3); zeros((800-120),1)];
       TWnsh = circshift(TWnon,280,1);
       rv = TWnsh.*rv;
       roughSc = 0.05;
     case 14
       load randstiff14.mat
           TWnon = [tukeywin(120,0.3); zeros((800-120),1)];
       TWnsh = circshift(TWnon,280,1);
       rv = TWnsh.*rv;
       roughScale = 0.05;
     case 15
       load randstiff15.mat
           TWnon = [tukeywin(120,0.3); zeros((800-120),1)];
       TWnsh = circshift(TWnon,280,1);
       rv = TWnsh.*rv;
       roughScale = 0.05;
     case 16
       load randstiff16.mat
       MaxS = 519;
       MinS = 418;
       TWnon = [tukeywin(MaxS-MinS,0.3); zeros((800-(MaxS - MinS)),1)];
       TWnsh = circshift(TWnon,MinS,1);
       rv = TWnsh.*rv;
       roughScale = 0.05;
     case 17
       load randstiff17.mat
       MaxS = 411;
       MinS = 312;
       TWnon = [tukeywin(MaxS-MinS,0.3); zeros((800-(MaxS - MinS)),1)];
       TWnsh = circshift(TWnon,MinS,1);
       rv = TWnsh.*rv;
       roughScale = 0.05;
     case 18
       load randstiff18.mat
       MaxS = 361;
       MinS = 258;
       TWnon = [tukeywin(MaxS-MinS,0.3); zeros((800-(MaxS - MinS)),1)];
       TWnsh = circshift(TWnon,MinS,1);
       rv = TWnsh.*rv;
       roughScale = 0.05;
     case 19
       load randstiff19.mat
       MaxS = 650;
       MinS = 130;
       %TWnon = [tukeywin(MaxS-MinS,1); zeros((800-(MaxS - MinS)),1)];
       TWnon = tukeywin(1600,1);
       TWnsh = TWnon(1:800);
       %TWnsh = circshift(TWnon,MinS,1);
       rv = TWnsh.*rv;
    case 20
       %load randstiff20.mat                    
       load randgaussstiff.mat % gaussian shaped peaks randomly distributed
%        rv = [];
%        gauss1 = gausswin(11,5);
%        for kg=1:16
%             gaussA = [gauss1; zeros((50-11),1)];
%             gaussA2 = circshift(gaussA,round((50-11)*rand(1)),1);
%             rv = [rv; gaussA2];
%        end
     case 21
       rv = 0;
       roughScale = 0;
 end
 
%stiff = stiff.*(1+roughScale*rv);
undamp = undamp.*(1+roughScale*rv);
 

NC=80; 
%Lp=fix((0.08+0.01*2)*fs);
Lp = fix(Tdur*fs);
t1=(0:Lp-1)*h;
Sig = zeros(size(t1));

Noct = log2(f2e/f2b); % give number of octaves
T = Noct/octpersec;

   
   
for k=1:length(t1)
    %[Sig(k)] = Inp_sigSrcSweeptME(t1(k),f2b,f2e,f2f1,octpersec,Lspl2,Lspl1,phi2s,phi1s,Rdur);
    [Sig(k)] = Inp_sigSrcChirpTondaME(f2b,f2e,Lspl1,Lspl2,f2f1,t1(k),phi1s,phi2s,T);
    %[Sig(k)]=Inp_sigSrcME(F1,A1,F2,A2,t1(k),S,phi1,phi2,T2on,T2off,T1off);
    
end

Ma1 = max(abs(Sig));
Ma1=Ma1*1.1;
Lp=length(t1);
%________________________Graphics_________________________________
 y=zeros(size(x));
if visual 
 H_FIG = figure('Name','HUMAN COCHLEA MODEL','DoubleBuffer','on','NumberTitle','off');
   H_BM=axes('Position', [.072 .1 .9 .4], 'Box', 'on','XLim',[x(1),x(N)],'YLim',[-Ma,Ma]);
      h_tit1 = text(-1, 0, 'BM displacement');
      set(H_BM, 'Title', h_tit1);
      h_L1 = line('XData',x,'YData',zeros(N,1),'Color', 'r');
           xlabel('Distance from stapes [cm]');
         
   H_SIG =	axes('Box', 'on','Position',[0.072,0.675,0.9,0.25]);
   set(H_SIG,'Xlim', [0, t1(Lp)], 'Ylim', [-Ma1, Ma1]);
   h_tit2 = text(-1, 0, ['Input signal-->F2b:=',num2str(f2b),' F2e:=',num2str(f2e) ' [Hz]'],'Tag','h_tit2');
   h_xlbl2 = text(-1,0, 'Time [sec]', 'Tag','h_xlbl2');
   h_ylbl2 = text(-1,0, 'Amplitude', 'Tag', 'h_ylbl2');
   set(H_SIG, 'Title', h_tit2,'XLabel', h_xlbl2, 'YLabel',h_ylbl2);
   h_L2 = line('XData',t1,'YData',Sig,'Erasemode','none','Color', 'b');
   h_L0=line('Xdata', [t1(1), t1(1)], 'Ydata', [-Ma1, Ma1]);   
   set(h_L0, 'Color', 'r');
   drawnow;
end
 %_________________________RUNGE KUTTA____________________________________%
 n_c=1;             %statistic for graphics 
 t0=0;              %start time
 y0=zeros(4*N+3,1);   %initial conditions
 y=y0(:);
 t=t0;
 neq = length(y);
 %------------------------------------------- 
 pow = 1/5;
 A = [1/5; 3/10; 4/5; 8/9; 1; 1];
 B = [
    1/5         3/40    44/45   19372/6561      9017/3168       35/384
    0           9/40    -56/15  -25360/2187     -355/33         0
    0           0       32/9    64448/6561      46732/5247      500/1113
    0           0       0       -212/729        49/176          125/192
    0           0       0       0               -5103/18656     -2187/6784
    0           0       0       0               0               11/84
    ];

F = zeros(neq,6);


hA = h * A;
hB = h * B;
%____________________Main routine___________________________________%
Y1FT=zeros(N,1);
Y2FT=zeros(N,1);
YDP1FT=zeros(N,1);
YDP2FT=zeros(N,1);
oae = zeros(Lp,1);
oaeSt = zeros(Lp,1);
tim = zeros(Lp,1);

count=0;
countDFT=0;
ind1=1;%find(x>0.8);
ind2=N;%find(x>2,1,'first');


while count<Lp   
                             
       F(:,1) = activeC(t,y);
       F(:,2) = activeC(t + hA(1), y + F*hB(:,1));
       F(:,3) = activeC(t + hA(2), y + F*hB(:,2));
       F(:,4) = activeC(t + hA(3), y + F*hB(:,3));
       F(:,5) = activeC(t + hA(4), y + F*hB(:,4));
       F(:,6) = activeC(t + hA(5), y + F*hB(:,5));
       
       %II=Inp_sigSrcME(F1,A1,F2,A2,t+hA(5),Sf,phi1,phi2);
     %  II=Inp_sigSrcME_Tub(F1,A1,F2,A2,t+hA(5),Sf,phi1,phi2,2e-3);
       dy=F*hB(:,6);
       
       %oae(count+1) = II - dy(4*N+3);
       oae(count+1) = F(4*N+3); 
       oaeSt(count+1)=Qs*dy(4*N+2)/h+Qbm*dy(N+1:2*N)/h;
      % oae(count+1)=Qbm*dy(N+1:2*N)/h;%????
      % tim(count+1) = Qs*II;
       
       tim(count+1)=t + hA(5);
               
       t = t + hA(6);
       y = y + F*hB(:,6);
       
                    
       if n_c==8&&visual %graphics
           set(h_L1,'Ydata',y(1:N));
           set(h_L0, 'Xdata', [t, t], ...
             'Color', 'r');    
           n_c=0;
           drawnow; 
       end
       n_c=n_c+1;
       count=count+1;
      
      
                       
end
if visual, close(H_FIG); end;


    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%----------------------------UTILITY----------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%-----------------------------------------------------------%%     
function ResetAll

 global Ginv DampSp  stiff bigamma wm2 undamp N MEinv kME hME Gme Sty gammaAir ...
        Ve GammaMi Gs Qbm Sow Wbm Pea
 
 global f2b f2e f2f1 octpersec Lspl2 Lspl1 phi2s phi1s Rdur
 
 Ginv=[]; DampSp=[]; stiff=[]; bigamma=[]; wm2=[];
 MEinv=[]; kME = []; hME = []; Gme = []; Sty=[]; gammaAir = []; Ve = []; 
 GammaMi = []; Gs = []; Qbm = []; Sow = []; Wbm = [];   Pea = [];
  
 undamp=[]; N=[];
 Rdur = []; Lspl1=[]; Lspl2=[];  f2b=[]; f2e=[]; phi1s=[]; phi2s=[];
 octpersec = []; f2f1 = [];
    
  
%---------------------------------------
function dXdV = passiveC(t,XV)

  global BMinput Ginv DampSp  stiff  N   
  global om om2 fac 
  
   inp=-tanh(t*fac)*om2*cos(om*t);
   X=XV(1:N);
   V=XV(N+1:2*N);

   dV = -BMinput*inp-Ginv*(stiff.*X + DampSp*V); 

   dXdV=[V
          dV];

%---------------------------------------
 function dXdVdYdW = activeC(t,XVYW)

global Ginv DampSp  stiff bigamma wm2 undamp N MEinv kME hME Gme Sty gammaAir ...
        Ve GammaMi Gs Qbm Sow Wbm Pea
    
global f2b f2e f2f1 octpersec Lspl2 Lspl1 phi2s phi1s Rdur
 
   %[inp]=Inp_sigSrcME(F1,A1,F2,A2,t,Sf,phi1,phi2,T2on,T2off,T1off);
   %inp = Inp_sigSrcSweeptME(t,f2b,f2e,f2f1,octpersec,Lspl2,Lspl1,phi2s,phi1s,Rdur);
   Noct = log2(f2e/f2b); % give number of octaves
   T = Noct/octpersec;

   inp = Inp_sigSrcChirpTondaME(f2b,f2e,Lspl1,Lspl2,f2f1,t,phi1s,phi2s,T);
   
      
    X=XVYW(1:N);
    V=XVYW(N+1:2*N);
    Y=XVYW(2*N+1:3*N);
    W=XVYW(3*N+1:4*N);
    Xme = XVYW(4*N+1);
    Vme = XVYW(4*N+2);
    

    Ycut=nonlin(Y);
%     Ycut=Y; %nonlin(Y;
    %dV = -BMinput*inp-Ginv*(stiff.*X + DampSp*V+ undamp.*Ycut); 
    
    dV = -1*Ginv*(stiff.*X + DampSp*V + undamp.*Ycut - Gs*MEinv*hME*Vme - Gs*MEinv*kME*Xme + Gs*MEinv*Sow*Gme*inp);
    dVme = -1*MEinv*(hME*Vme + kME*Xme + Sow*Qbm*dV/Wbm - Sow*Gme*inp);
    
    Pe = inp - gammaAir*Pea*Sty*GammaMi/Ve*Xme;
        
    dXdVdYdW=[ V                          %N
               dV                         %2N 
               W                          %3N
               -bigamma.*W - wm2.*Y - dV  %4N
               Vme                        %4N+1   
               dVme                       %4N+2 
               Pe                         %4N+3
                ];            


 
