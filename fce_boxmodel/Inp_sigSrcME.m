function [Sig]=Inp_sigSrcME(F1,A1,F2,A2,t,phi1,phi2,T2on,T2off,T1off)
% function [Sig]=Inp_sigSrcME(F1,A1,F2,A2,t,S,phi1,phi2,T2on,T2off,T1off)
% generate input signal (two tone stimulus)
% f1 tone starts always at t = 0sec., onset of the f2 tone can be
% manipulated by T2on
% F1 - frequency (Hz) of the f1 tone (lower frequency)
% F2 - frequency (Hz) of the f2 tone (higher frequency)
% A1 - amplitude (dB) of the f1 tone
% A2 - amplitude (dB) of the f2 tone
% t - time (seconds)
% phi1 - phase (rad) of the f1 tone
% phi2 - phase (rad) of the f2 tone
%

om1=2*pi*F1; AM1=db2inputME(A1);% get amplitudes
om2=2*pi*F2; AM2=db2inputME(A2);% get amplitudes
durHWF2 = 4e-3; % duration of F2 ramp % shaping onset and offset of tones
durHWF1 = 4e-3; % duration of F1 ramp


if t<T2on  
    Sig=m_hann(t,durHWF1)*AM1*cos(om1*t+phi1);
elseif (t>=T2on)&&(t<T2off-durHWF2)
    Sig=m_hann(t,durHWF1)*AM1*cos(om1*t+phi1)+ m_hann(t-T2on,durHWF2)*AM2*cos(om2*t+phi2);
elseif (t>=T2off-durHWF2)&&(t<T2off) 
    Sig=m_hann(t,durHWF1)*AM1*cos(om1*t+phi1)+m_hann(T2off-t,durHWF2)*AM2*cos(om2*t+phi2);
elseif (t>=T2off)&&(t<T1off-durHWF1)
    Sig=AM1*cos(om1*t+phi1);
elseif (t>=T1off-durHWF1)&&(t<T1off)
    Sig=m_hann(T1off-t,durHWF1)*AM1*cos(om1*t+phi1);
else
    Sig=0;
end


     
 
function w=m_hann(t,dur)
% hann windows
      framp = 1/(2*dur);
      if t<=dur
        w = 0.5 * (1 - cos(2*pi*t*framp));  
      else
          w = 1;
      end