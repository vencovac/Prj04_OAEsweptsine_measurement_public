function [H,K_L,Gs,x] = GreenNPTM(gain,F,TMres)
%function [H,K_L,Gs,x]=GreenNPTM(gain,F,TMres);
% 
% 
% ME + Cochlea
%
% F: Frequency in Hz
% 
if nargin <3, TMres = 0.45; end
if nargin<2, F=1500; end
if nargin <1, gain=1.3; end
 
 
%   gain = 1.05;

  N=800;
  rebuild_flag=1; 
  
  [x,Gs,G,M,stiff,DampSp,undamp,bigamma,wm2,Qbm,Qs,mME,kME,hME,Gme,Sty,...
    gammaAir,Ve,GammaMi,Sow,W,Pea] = AlldataFmeTM(N,rebuild_flag,gain,TMres);
  
  
  Gs=Gs(:);
  %U=zeros(N,1);U(IND-20:IND+20)=AM;
%  U=Gs*AM;
  I=sqrt(-1);omega=2*pi*F;omega_2=omega*omega;
%  AM=db2inputME(Inp);%*omega^2;
  Bh=DampSp;%./omega;
  Bk=diag(stiff);%./omega_2);
  %-----------------------------
  % Vetesnik Gummer 2012
  %Gme=Gs*G(1,:);
%   fac=1;
%   Ke=fac*3.2400e+009; %dyn/cm^5
%   Re=fac*4e5;   %dyn-s/cm^5;
%   Me=fac*17.6;   %4.4 g/cm^4; 
%   As=0.03;
%   
  %M3=As*(Ke/omega_2+I*Re/omega-Me)-Gs(1);%
  %Gmew=(1/M3)*Gme/100;

  % Vencovsky Vetesnik 2017
  % middle-ear model according to Talmadge
  Zme = mME + Sow*Qs/W(1) - I*hME/omega-kME/omega_2; % middle ear
    
  
%% z SFOAE kodu
%   Zme = mME + Sow/W(1)*Qs - I*hME/omega - kME/omega_2;
%   
%    % TM
%   Th=I*omega*bigamma;Tk=wm2;TM=diag(undamp./(omega_2-Th-wm2));
%   
% K=-(G+M-Sow/W(1)*Gs*Qbm/Zme)*omega_2+I*Bh*omega+Bk - TM*omega_2 ;
%   
%   % TM
% %  Th=I*omega*bigamma;Tk=wm2;TM=diag(undamp./(omega_2-Th-wm2));
%   
%   %K = G+M-Sow/W(1)*Gs*Qbm/Zme - I*Bh-Bk;
%   
%   Y=K\(-U1.'+U2.' - dstiff.*RR1.' + dstiff.*RR2.');
%   
%   Kr=-(G+M-Sow/W(1)*Gs*Qbm/Zme)*omega_2+I*Bh*omega+Bk + diag(dstiff) - TM*omega_2 ;
%   
%   Pd = Y(1)*Kr(1,1)/W(1);
%   
  
  %MhMe = Gs/(mOW+Gs(0));
  %Gmew=MhMe*G(1,:);
  %GME = Sow*Gs*Qbm/(W(1)*Zme);
  
  %undamp(300:310) = 0;
  
  %stiffR = stiff.*(1+r_amp*rv);
  % dstiff = stiffR - stiff;
  
%  load randstiff17.mat %nahraje rv
%  MaxS = 411;
%  MinS = 312;
%  TWnon = [tukeywin(MaxS-MinS,0.3); zeros((800-(MaxS - MinS)),1)];
%  TWnsh = circshift(TWnon,MinS,1);
%  rv = TWnsh.*rv;
%  
%  roughScale = 0.05;
%  undampr = undamp.*(1+roughScale*rv);
%  dundamp = undampr - undamp;
  
  
 Th=I*omega*bigamma;Tk=wm2;TM=diag(undamp./(omega_2-Th-wm2)); % TM (linear amplification)
 %dTM = diag(dundamp./(omega_2-Th-wm2)); % delta TM
  %A= M + G - GME - I*Bh - Bk + TM;
  
 K_L=-(G+M-Sow/W(1)*Gs*Qbm/Zme)*omega_2+I*Bh*omega+Bk - TM*omega_2; 
 
 H=zeros(N,N);
 
 for i=1:N
 
% dK_L = - dTM*omega_2;
 
  Input = zeros(size(Gs));
  Input(i)=1;
  H(i,:)=(K_L)\(Input);
 end
