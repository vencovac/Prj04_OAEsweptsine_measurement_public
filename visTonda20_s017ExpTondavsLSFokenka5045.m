% calculate DPOAE from experimental data
% purpose is to compare DPOAEs estimated with least-square fitting and dpoaes
% estimated by inverse filtering method
% data for s015, L1 = 50, L2 = 45 dB

close all;
build = 1;

addpath('fce_synchsweptsine/');
addpath('fce_lsf/');
addpath('fce_oaev2/');

if build
    
clear all;
% addpath('fce_waveletdecomposition\');


%% load experimentally measured sweep responses
listfiles = dir('Experiments/s017/'); 
numR = 0;
recMean = 0;
octperseco = 0.5;
recNm = [];
RMEdelay = 3146;  % latency of RME soundcard
% RMEdelay = 1500;

for k = 1:length(listfiles)-2
    if contains(listfiles(k+2).name,'105058')
        load([listfiles(k+2).folder '/' listfiles(k+2).name]);
        
        % process rec signal
        fcutoff =300; % cutoff freq of 2order high pass butterworth filter
        [b,a] = butter(4,fcutoff/(fs/2),'high');
        recP = filtfilt(b,a,recordedSignal(:,1));
        
        % make a matrix (assume more than one response)
        numR = numR + 1;
        recMatrix(numR,:) = recP.';
                
%         params.f2f1 = f2f1;
%         params.f1 = F2start; % logsweep start frequency  (for 'tone' type, only f1 is necessary)
%         params.f2 = F2stop; % logsweep end frequency    
%         params.D = 3146;
%         params.fs = 44.1e3; % sampling rate (default = 44100);
%         params.octpersec = octperseco;
%         params.nfilt = 2^16; % window size (1.48s, recommended for 0.5 oct/s)
%         params.nstep = round(params.nfilt/2); % step size
%         params.GainMicA = GainMicA;
%         params.TukWinR = 0.3;
%         params.tau01 = 20e-3;
%         
%         if k == 1
%             [DPOAEcalib, NFLOORcalib, DPOAEcalibNL, NFLOORcalibNL, DPOAEcalibCR, fxI, yp] = show_DPgramSynchSS(recP,params);
%         else
%             [DPOAEcalib, NFLOORcalib, DPOAEcalibNL, NFLOORcalibNL, DPOAEcalibCR, fxI, yp] = show_DPgramSynchSS(recP,params,yp);
%         end
%         
%         %numR = numR + 1;
%         %recMean = recMean + recP;
%         
%         % a matrix for noise estimation
%         %if mod(numR,2)==1
%         %    recN = recP;
%         %else
%         %    recNm = [recNm recN(RMEdelay:end)-recP(RMEdelay:end)]; 
%         %end
    end
    
end

% remove sound card delay

recMatrix = recMatrix(:,RMEdelay:end);


% yi-wen noise estimation and sample rejection

% there are some previously recorded samples available, hence focus on
% the method provided by the Yi-Wen Liu group (2022)
% 1 calculate mean of the recorded responses

recMean = median(recMatrix);

% 2. obtain noise matrix
noiseM = recMatrix - repmat(recMean,numR,1);
Nsamp = length(recMean); % number of samples

sigma = sqrt(mean(1/Nsamp*sum(noiseM.^2,2)));
Theta = 2*sigma; % estimation of the threshold for sample removal

recMatrix(abs(noiseM)>Theta) = NaN;
noiseM(abs(noiseM)>Theta) = NaN;

oaeDS = nanmean(recMatrix).';  % exclude samples set to NaN (noisy samples)
nfloorDS = nanmean(noiseM).';


%% SSS technique

paramsSSS.f2f1 = f2f1;
paramsSSS.f1 = F2start; % logsweep start frequency  (for 'tone' type, only f1 is necessary)
paramsSSS.f2 = F2stop; % logsweep end frequency
paramsSSS.D = 1;
paramsSSS.fs = 44.1e3; % sampling rate (default = 44100);
paramsSSS.octpersec = octperseco;
paramsSSS.nfilt = 2^16; % window size (1.48s, recommended for 0.5 oct/s)
paramsSSS.GainMicA = GainMicA;

[dpoaeTonda, nfloorTonda, dpoaeTondaNL, nfloorTondaNL, dpoaeTondaCR, fxI] = estimateDPOAE_SStechnique(oaeDS,nfloorDS,paramsSSS);

f2vect = f2f1*fxI/(2-f2f1);  % convert fdp freq to f2 freq
fdptonda = fxI;
%oae
% DPOAEdB = 20*log10(abs(DPOAEcal)/(2e-5*sqrt(2)));
% %         DPtdB = 20*log10(abs(DPt)/2e-5/sqrt(2));
% NoiseFloordB = 20*log10(abs(NoiseFloorPa)/(2e-5*sqrt(2)));
% %         DPtNdB = 20*log10(abs(DPtN)/2e-5/sqrt(2));
% 
% cycle = 2*pi;
% DPOAEPh = unwrap(angle(DPOAEcal))/cycle;



%% least square fitting method

f1 = F2start;
f2 = F2stop;
%T = length(oaeDS)*1/fs;
Noct = log2(f2/f1); % give number of octaves
T = Noct/octperseco;

f2f1 = 1.2;
paramsLSF.windowType = 2;                      % window function (-1 = rectangular, 0 = welch, 1 = hamming, 2 = hann [default], 3 = parzen, 4 = blackman)
paramsLSF.fs = 44.1e3;                          % sampling rate (default = 44100);
paramsLSF.f2f1 = f2f1;
paramsLSF.T = T;
paramsLSF.nfilt = 5512;                        % window size (125ms, recommended for 0.5 oct/s)
paramsLSF.nstep = 200;                          % step size
paramsLSF.component(1).type = 'logsweep';      % component type (values can be 'logsweep', 'tone', 'linear', 'custom'; for 'custom' a phase function, pf needs to be defined)
coef1 = 1.2;            
fdp1 = 2*f1/coef1 - f1;
fdp2 = 2*f2/coef1 - f2;

paramsLSF.component(1).f1 = fdp1;
paramsLSF.component(1).f2 = fdp2;
paramsLSF.nskip = 0;% 1.26e4;                         % samples to skip, default = 0
oae = nseoae(oaeDS.',paramsLSF);

% noise floor estimation
oaeN = nseoae(nfloorDS.',paramsLSF);

% LSF with longer window:
paramsLSF.nfilt = 22050;                        % window size (125ms, recommended for 0.5 oct/s)
oaeNL = nseoae(oaeDS.',paramsLSF);
oaeNNL = nseoae(nfloorDS.',paramsLSF);

oaelsf = zeros(size(oae.f));
nfloorlsf = zeros(size(oae.f));


for kf=1:length(oae.f)
    oaelsf(kf) = giveTrueSPL(oae.f(kf),oae.mag(kf).*exp(sqrt(-1)*oae.phase(kf)),GainMicA,fs,0);
    
    nfloorlsf(kf) = giveTrueSPL(oae.f(kf),oaeN.mag(kf),GainMicA,fs,0);
        
end

oaelsfNL = zeros(size(oaeNL.f));
nfloorlsfNL = zeros(size(oaeNL.f));

for kf=1:length(oaeNL.f)
    oaelsfNL(kf) = giveTrueSPL(oaeNL.f(kf),oaeNL.mag(kf).*exp(sqrt(-1)*oaeNL.phase(kf)),GainMicA,fs,0);
    
    nfloorlsfNL(kf) = giveTrueSPL(oaeNL.f(kf),oaeNNL.mag(kf),GainMicA,fs,0);

end


% % convert to Pascals and limit to the range for lsf method
% idxMin1 = find(fx>=min(oae.f),1,'first');
% idxMax1 = find(fx>=max(oae.f),1,'first');
% fdptonda = fx(idxMin1:idxMax1); % freq axis (fdp) for tonda method 
% oaeSWAll = HmAll(idxMin1:idxMax1);  % for overall signal
% noiseFlTAll = oaeNoiseTondaAll(idxMin1:idxMax1);
% % find freq range for conversion to Pascals for windowed data
% 
% idxMinW1 = find(~isnan(YspWin)==1,1,'first');
% idxMaxW1 = find(~isnan(YspWin)==1,1,'last');
% fdptondaWin = fxwin(idxMinW1:idxMaxW1);
% oaeSWWin = YspWin(idxMinW1:idxMaxW1);
% %oaeSW100 = Hm100(idxMin1:idxMax1);
% noiseFlTWin = oaeNoiseTondaWin(idxMinW1:idxMaxW1);
% % noiseFlT100 = oaeNoiseTonda100(idxMin1:idxMax1);
% 
% oaetondaAll = zeros(size(fdptonda));
% oaetondaWin = zeros(size(fdptondaWin));
% nfloortondaAll = zeros(size(fdptonda));
% nfloortondaWin = zeros(size(fdptondaWin));
% 
% for kf=1:length(fdptonda)
%     oaetondaAll(kf) = giveTrueSPL(fdptonda(kf),oaeSWAll(kf),GainMicA,fs,0);
%     oaetonda100(kf) = giveTrueSPL(fdptonda(kf),oaeSW100(kf),GainMicA,fs,0);
    
%     nfloortondaAll(kf) = giveTrueSPL(fdptonda(kf),noiseFlTAll(kf),GainMicA,fs,0);
%     oaetonda
%     nfloortonda100(kf) = giveTrueSPL(fdptonda(kf),noiseFlT100(kf),GainMicA,fs,0);
% end

% for kf=1:length(fdptondaWin)
%     oaetondaWin(kf) = giveTrueSPL(fdptondaWin(kf),oaeSWWin(kf),GainMicA,fs,0);
%     oaetonda100(kf) = giveTrueSPL(fdptonda(kf),oaeSW100(kf),GainMicA,fs,0);
    
%     nfloortondaWin(kf) = giveTrueSPL(fdptondaWin(kf),noiseFlTWin(kf),GainMicA,fs,0);
    
    % backNoiseLSF = nseoae(addnoise.',params);
% end

%freq = oae.f./(2 - f2f1);

k = 1;
LW = 1.1;
MS = 8;



parW.nmm=25;
parW.nb=6; % n. 1/3 oct bands
parW.tt=13; % latency power law: tt*f(kHz)^-bbb
parW.bbb=1;
parW.p0=1;
parW.tc=-50;
parW.tch=50;
parW.c1=-0.4;
parW.c2=0.4;
parW.c3=4.;
parW.nmm=25;
parW.nb=6; % n. 1/3 oct bands
parW.tt=13; % latency power law: tt*f(kHz)^-bbb
parW.bbb=1;
parW.p0=1;
parW.tc=-50;
parW.tch=50;
parW.c1=-0.3;
parW.c2=0.3;
parW.c3 = 1;
parW.c4=2.5;
parW.c5=3;
plotflag = 1;

%[fx, DPgAlla, DPgNLaWLT, DPgCRa, DPgCR1a, DPgAllreca, DPgAllph, DPgNLph, DPgCRph, DPgCR1ph, DPgAllrecph, ...
%    fc, DPtoctAlla, DPtoctNLa, DPtoctCRa, wlgraph] = WLTdecompFCE(parW,oae.f,oaelsf,plotflag);

% dpoaeTondaInt = interp1(fdptonda,dpoaeTonda,oae.f,'spline');
% [fx, DPgAlla, DPgNLa, DPgCRa, DPgCR1a, DPgAllreca, DPgAllph, DPgNLph, DPgCRph, DPgCR1ph, DPgAllrecph, ...
%     fc, DPtoctAlla, DPtoctNLa, DPtoctCRa, wlgraph] = WLTdecompFCE(parW,oae.f,dpoaeTondaInt,plotflag);
% parW.c4=2.5;
% parW.c4=6;
% parW.c5=6;




end

cycle = 2*pi;

figure;

set(gcf, 'Units', 'centimeters');
% we set the position and dimension of the figure ON THE SCREEN
%
% NOTE: measurement units refer to the previous settings!
afFigurePosition = [1 1 20 12];
% [pos_x pos_y width_x width_y]
set(gcf, 'Position', afFigurePosition); % [left bottom width height]
% we link the dimension of the figure ON THE PAPER in such a way that
% it is equal to the dimension on the screen
%
% ATTENTION: if PaperPositionMode is not ’auto’ the saved file
% could have different dimensions from the one shown on the screen!
set(gcf, 'PaperPositionMode', 'auto');

iFontSize = 12;
strFontName = 'Helvetica';
LWtick = 1.3;
LW = 1.3;
MS = 8;
leftCornerX = 0.1;
leftCornerY = 0.54;
wEnv = 0.4;
hEnv = 0.41;
Xlims = [20 75];
Ylims = [30 75];
Clims = [0 800];
Clims = [0 60];

haAbs10 = axes('position',[leftCornerX leftCornerY  wEnv hEnv],'xscale','log','yscale','lin','nextplot','add');

idxSSS1 = find(fdptonda>=700,1,'first');
idxSSS2 = find(fdptonda>=8e3,1,'first');
fdptonda = fdptonda(idxSSS1:idxSSS2);
dpoaeTonda = dpoaeTonda(idxSSS1:idxSSS2);
nfloorTonda = nfloorTonda(idxSSS1:idxSSS2);
l1 = plot(fdptonda/1e3,20*log10(abs(dpoaeTonda)/(sqrt(2)*2e-5)),'color',[0 0 0],'linewidth',LW+0.22*k);
l2 = plot(fdptonda/1e3,20*log10(abs(nfloorTonda)/(sqrt(2)*2e-5)),'color',[0.6 0.6 0.6],'linewidth',LW-0.32*k,'linestyle',':');
l3 = plot(oae.f/1e3,20*log10(abs(oaelsf)/(sqrt(2)*2e-5)),'color',[1 0 0],'linewidth',LW+0.22*k,'linestyle','--');
l4 = plot(oae.f/1e3,20*log10(abs(nfloorlsf)/(sqrt(2)*2e-5)),'color',[1 0 0],'linewidth',LW-0.32*k,'linestyle',':');

set(haAbs10,...
    'xlim',[700 8e3]/1000,...
    'ylim',[-30 23  ],...
    'xticklabel','',...
    'box','on',...
    'fontsize',iFontSize,...
    'fontname',strFontName...
   );

text(0.45,-4,'Amplitude (dB SPL)','fontsize',iFontSize+1,'fontname',strFontName,'rotation',90,'horizontalalignment','center')

text(0.86,17,'A-1','fontsize',iFontSize+1,'fontname',strFontName,'rotation',0,'horizontalalignment','center','fontweight','bold')

haPh10 = axes('position',[leftCornerX leftCornerY-hEnv  wEnv hEnv],'xscale','log','yscale','lin','nextplot','add');
% idxSSstart = find(fxdpSW>670,1,'first');

l1 = plot(fdptonda/1e3,unwrap(angle(dpoaeTonda))/(cycle)-1,'color',[0 0 0],'linewidth',LW+0.22*k)
l2 = plot(fdptonda/1e3,20*log10(abs(nfloorTonda)/(sqrt(2)*2e-5))-100,'color',[0.6 0.6 0.6],'linewidth',LW-0.32*k,'linestyle',':')
l3 = plot(oae.f/1e3,unwrap(angle(oaelsf))/cycle-1,'color',[1 0 0],'linewidth',LW+0.22*k,'linestyle','--')
l4 = plot(oae.f/1e3,20*log10(abs(n




% figure;
% 
% set(gcf, 'Units', 'centimeters');
% % we set the position and dimension of the figure ON THE SCREEN
% %
% % NOTE: measurement units refer to the previous settings!
% afFigurePosition = [1 1 10 7];
% % [pos_x pos_y width_x width_y]
% set(gcf, 'Position', afFigurePosition); % [left bottom width height]
% % we link the dimension of the figure ON THE PAPER in such a way that
% % it is equal to the dimension on the screen
% %
% % ATTENTION: if PaperPositionMode is not ’auto’ the saved file
% % could have different dimensions from the one shown on the screen!
% set(gcf, 'PaperPositionMode', 'auto');
% 
% iFontSize = 12;
% strFontName = 'Helvetica';
% LWtick = 1.3;
% LW = 0.8;
% MS = 8;
% leftCornerX = 0.17;
% leftCornerY = 0.18;
% wEnv = 0.78;
% hEnv = 0.78;
% Xlims = [20 75];
% Ylims = [30 75];
% Clims = [0 800];
% Clims = [0 60];
% 
% haImp = axes('position',[leftCornerX leftCornerY  wEnv hEnv],'xscale','lin','yscale','lin','nextplot','add');
% 
% tx_imp = linspace(-len_IR/fs,len_IR/fs,len_IR);
% plot(tx_imp/1e-3, hm_puvodni/max(abs(hm_puvodni)),'color','k'); 
% plot(tx_imp/1e-3, hm_puvodni/max(abs(hm_puvodni)),'color','k','linestyle','--'); 
% plot(tx_imp/1e-3,w,'color',[.4 .4 .4])
% plot(tx_imp/1e-3,w2,'color',[.7 .7 .7],'linestyle','--');
% 
% 
% 
% set(haImp,...
%     'xlim',[-len_IR/fs len_IR/fs]*1e3,...
%     'ylim',[-1 1],...
%     'box','on',...
%     'fontsize',iFontSize,...
%     'fontname',strFontName...
%    );
% 
% ylabel('Normalized amplitude','fontsize',iFontSize+1,'fontname',strFontName);
% 
% %legend('Swept-tone DPOAE','Discrete-tone DPOAE', 'position',[0.64 0.55 0.2 0.05]);
% 
% xlabel('Time (ms)','fontsize',iFontSize+1,'fontname',strFontName)
% 
% 
% %
% strFileName = ['Figures/ImpResp'];
% print('-depsc', strcat(strFileName, '.eps'));
% 
% 
% 
% rmpath('fce_synchsweptsine/');
% rmpath('fce_lsf/');
% rmpath('fce_waveletdecomposition/');
% 
% 
% 
% 
% % figure;
% % subplot(2,1,1);
% % hold on;
% % plot(fx,20*log10(abs(Hm)))
% % plot(Fdpvect,20*log10(abs(oaedp)));
% % %plot(fx,20*log10(abs(Hmnoise)),'k--','linewidth',0.8);
% % xlim([500 2.7e3]);
% % subplot(2,1,2);
% % plot(fx,unwrap(angle(Hm)-2*pi*fx'*len_IR/2/fsD)/(2*pi))
% xlim([500 2.7e3]);floorlsf)/(sqrt(2)*2e-5))-100,'color',[1 0 0],'linewidth',LW-0.32*k,'linestyle',':');
text(6,-5,'A-2','fontsize',iFontSize+1,'fontname',strFontName,'rotation',0,'horizontalalignment','center','fontweight','bold')
set(haPh10,...
    'xlim',[700 8e3]/1000,...
    'ylim',[-32 -0.01],...
    'xtick',[1 2 5 10],...
    'box','on',...
    'fontsize',iFontSize,...
    'fontname',strFontName...
   );

xlabel('Frequency {\itf}_{\rmDP} (kHz)','fontsize',iFontSize+1,'fontname',strFontName)

text(0.45,-16,'Phase (cycles)','fontsize',iFontSize+1,'fontname',strFontName,'rotation',90,'horizontalalignment','center')
%

legend([l1,l2,l3,l4],{'SSS','NF SSS','LSF','NF LSF'}, 'position',[0.11 0.145 0.15 0.19]);


haAbs100 = axes('position',[leftCornerX+wEnv leftCornerY  wEnv hEnv],'xscale','log','yscale','lin','nextplot','add');

dpoaeTondaNL = dpoaeTondaNL(idxSSS1:idxSSS2);
nfloorTondaNL = nfloorTondaNL(idxSSS1:idxSSS2);
dpoaeTondaCR = dpoaeTondaCR(idxSSS1:idxSSS2);
oaelsfR = interp1(oae.f,oaelsf,oaeNL.f);
oaelsfCR = oaelsfR - oaelsfNL;

plot(fdptonda/1e3,20*log10(abs(dpoaeTondaNL)/(sqrt(2)*2e-5)),'color',[0 0 0],'linewidth',LW+0.22*k)
plot(fdptonda/1e3,20*log10(abs(dpoaeTondaCR)/(sqrt(2)*2e-5)),'color',[0.6 0.6 0.6],'linewidth',LW+0.22*k)
plot(oaeNL.f/1e3,20*log10(abs(oaelsfCR)/(sqrt(2)*2e-5)),'color',[0.8500, 0.3250, 0.0980],'linewidth',LW+0.22*k,'linestyle','--');
plot(fdptonda/1e3,20*log10(abs(nfloorTondaNL)/(sqrt(2)*2e-5)),'color',[0.6 0.6 0.6],'linewidth',LW-0.32*k,'linestyle',':')
plot(oaeNL.f/1e3,20*log10(abs(oaelsfNL)/(sqrt(2)*2e-5)),'color',[1 0 0],'linewidth',LW+0.22*k,'linestyle','--');
plot(oaeNL.f/1e3,20*log10(abs(nfloorlsfNL)/(sqrt(2)*2e-5)),'color',[1 0 0],'linewidth',LW-0.32*k,'linestyle',':');
text(0.86,17,'B-1','fontsize',iFontSize+1,'fontname',strFontName,'rotation',0,'horizontalalignment','center','fontweight','bold')

set(haAbs100,...
    'xlim',[700 8e3]/1000,...
    'ylim',[-30 23  ],...
    'xticklabel','',...
    'yticklabel','',...
    'box','on',...
    'fontsize',iFontSize,...
    'fontname',strFontName...
   );

%legend('SSS windows','SSS noise floor','LSF','LSF noise floor', 'position',[0.65 0.88 0.25 0.07]);

haPh100 = axes('position',[leftCornerX+wEnv leftCornerY-hEnv  wEnv hEnv],'xscale','log','yscale','lin','nextplot','add');
% idxSSstart = find(fxdpSW>670,1,'first');

l1 = plot(fdptonda/1e3,unwrap(angle(dpoaeTondaNL))/(cycle)-1,'color',[0 0 0],'linewidth',LW+0.22*k)
l2 = plot(fdptonda/1e3,20*log10(abs(nfloorTondaNL)/(sqrt(2)*2e-5))-100,'color',[0.6 0.6 0.6],'linewidth',LW-0.32*k,'linestyle',':')
l3 = plot(oaeNL.f/1e3,unwrap(angle(oaelsfNL))/cycle-1,'color',[1 0 0],'linewidth',LW+0.22*k,'linestyle','--')
l4 = plot(oae.f/1e3,20*log10(abs(nfloorlsf)/(sqrt(2)*2e-5))-100,'color',[1 0 0],'linewidth',LW-0.32*k,'linestyle',':');
l5 = plot(fdptonda/1e3,unwrap(angle(dpoaeTondaCR))/(cycle),'color',[0.6 0.6 0.6],'linewidth',LW+0.22*k)
l6 = plot(oaeNL.f/1e3,unwrap(angle(oaelsfCR))/cycle,'color',[0.8500, 0.3250, 0.0980],'linewidth',LW+0.22*k,'linestyle','--')
text(6,-5,'B-2','fontsize',iFontSize+1,'fontname',strFontName,'rotation',0,'horizontalalignment','center','fontweight','bold')
set(haPh100,...
    'xlim',[700 8e3]/1000,...
    'ylim',[-32 -0.01],...
    'xtick',[1 2 5 10],...
    'yticklabel','',...
    'box','on',...
    'fontsize',iFontSize,...
    'fontname',strFontName...
   );

% legend([l1,l2,l3,l4],{'SSS (SL)','SSS (SL) noise floor','LSF 500 ms','LSF 500 ms noise floor'}, 'position',[0.52 0.143 0.25 0.18]);
legend([l1,l2,l3,l4,l5,l6],{'SSS (SL)','NF SSS (SL)','LSF (SL)','NF LSF (SL)',...
    'SSS (LL)','LSF (LL)'}, 'position',[0.36 0.145 0.34 0.19],'NumColumns',2);

xlabel('Frequency {\itf}_{\rmDP} (kHz)','fontsize',iFontSize+1,'fontname',strFontName)


strFileName = ['Figures/Figure08'];
exportgraphics(gcf, strcat(strFileName, '.eps'),'ContentType','vector');
exportgraphics(gcf, strcat(strFileName, '.png'));


rmpath('fce_synchsweptsine/');
rmpath('fce_lsf/');
% % rmpath('fce_waveletdecomposition\');
rmpath('fce_oaev2/');