function [Hm, fx, hm, tx, hmnoise] = fceDPOAEinwinSS(oaeDS,Npts,f1s,L1sw,rF,fsD,tau01,tau02,tshift)
% function [Hm, fx, hm, tx, hmnoise] = fceDPOAEinwinSS(oaeDS,Npts,f1s,L1sw,rF,fsD,tau01,tau02,tshift)
%
% calculates DPOAE with SSS technique
% 
% oaeDS - time domain signal from which DPOAE is extracted (response to
% swept sines in the ear canal)
% Npts - number of samples in oaeDS
% fs1 - starting frequency of the synch. swept sine (the initial frequency
% regardless windowing)
% L1sw - parameter for the swept sine: L1sw = T/log(f2/f1) (for the overall
% swept sine)
% rF - ratio of the f2/f1 frequencies used to evoke DPOAE
% fsD - sampling frequency
% tau01 - tau parameter (cutoff) for first half of the recursive exponential function
% tau01 - tau parameter (cutoff) for the second half of the recursive exponential function
% region around the DPOAE impulse response
% tshift - time shift (for the swept sine response in the windows)

% calculate spectrum of synchronized swept sine (shifted version in case of
% time windows which does not start at 0 time)
[S,f] = synchronized_swept_sine_spectra_shifted(f1s,L1sw,fsD,Npts,tshift);

% spectra of the output signal
Y = fft(oaeDS)./fsD;
% frequency-domain deconvolution
H = Y./repmat(S,1,size(oaeDS,2)); h = ifft(H, 'symmetric');

% -- separation of IMD harm
rF1 = 2 - rF;
dt = -fsD*L1sw.*log(rF1);     % positions of the selected (coef2) IMD component [samples]


% -- Higher Harmonic Impulse Responses
dt_ = round(dt);
dt_rem = dt - dt_;
len_IR = 2^11; % length of the impulse response window (adequate for the used time windows for DPOAEs)
hm = h(dt_-len_IR/2+1:dt_+len_IR/2);
hmnoise = h(dt_-2*len_IR/2+1:dt_-len_IR/2); % background noise (between 2f1-f2 and 3f1 - 2f2)
% background noise to estimate impulse noise during
% frequency axis definition (0 .. 2pi)
axe_w = linspace(0,2*pi,len_IR+1)'; axe_w(end) = [];

% Non-integer sample delay correction
Hx = fft(hm).* exp(-1i*dt_rem*axe_w);
hm = ifft(Hx,'symmetric');

% -- Higher Harmonic Frequency Responses
Nwindow = 10; % degree of the Kaiser windows
%w = kaiser(length(hm), Nwindow);
%     w = roexwin(length(hm),10,fsD,1e-3,20e-3);
w = roexwin(length(hm),Nwindow,fsD,tau01,tau02);

%     hm_puvodni = hm;
hm = hm.*w;

Hm = fft(fftshift(hm));

tx = 0:1/fsD:(Npts-1)/fsD;
%     Hmnoise = fft(hmnoise);
%     Hm2 = fft(fftshift(hm2));
fx = (0:length(Hm)-1)*fsD/length(Hm); % frequency axis

    