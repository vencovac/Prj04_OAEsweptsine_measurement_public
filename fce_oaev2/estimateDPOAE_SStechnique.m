function [DPOAEcalib, NFLOORcalib, DPOAEcalibNL, NFLOORcalibNL, DPOAEcalibCR, fxI] = estimateDPOAE_SStechnique(recordedSignal,nfloorEst,params)


fs = params.fs;
rF = params.f2f1;
GainMicA = params.GainMicA;
dt = 1/fs;
oaeDS = recordedSignal(params.D:end,1);
nfloorDS = nfloorEst(params.D:end,1);

Nsamp = length(oaeDS); % number of samples in the signal

Fpstart = params.f1;      % final f1 frequency
Fpstop = params.f2;       % final f2 frequency
twin = params.nfilt/fs;    % win size in samples

T = log2(Fpstop/Fpstart)/params.octpersec;     % duration of the sweep given by freq range and speed (oct/sec)

f1s = params.f1/rF; % get F1 frequencies
f1e = params.f2/rF;
Nwin = round(twin*fs); % number of samples in a window
Nwinpul = round(Nwin/2); % half the number of samples in a window

L1sw = T/log(f1e/f1s); % overall L1sw parameter for synchronized swept sine

finst = inst_freq_synch_swept_sine(f1s,f1e,T,fs);  % instantaneous frequency for swept sine
% return frequency in Hz for the swept f1 tone
fc = finst(Nwinpul:Nwin:end); % find center frequencies for recursive exponential window sizes
fdpc = 2*fc - rF*fc; % convert center freq from f1 to fdp (assumed cdt (low-side))

aW = 0.01;
tauNL = 0.5*aW*(fdpc/1000).^(-0.8);  % NL window width as a function of fdp
tauAll = 5/2*aW*(fdpc/1000).^(-0.8); % overall window width (NL + CR components) as a function of fdp

% tauNL = 1.2*fdpc.^(-0.8);  % window sizes for NL component only (short latency)
% tauAll = 7*fdpc.^(-0.8);  % window sizes for NL + LL components (short + long latency)
% exponent -0.8 chosen according to Moleti et al. wavelet filters
tau01 = 1e-3; % tau for the first half of the roex windows (the part for negative times)
% note: the final roex window is assymetrical, 1ms part followed with a
% part given by tauNL or tauAll

%fx = fs*(0:(nfilt/2))/nfilt; % freq axis for spectrum

%fvecNF = zeros(ntotal,1);
%DPOAEdB = zeros(ntotal,1);
%NoiseFloordB = zeros(ntotal,1);
%First = 0;
tshift = 0; % always at time zero is the begining of the window

for k=1:length(fdpc)  % estimate for individual roex windows
    [Hm(:,k), fxfdp, hm(:,k), ~] = fceDPOAEinwinSS(oaeDS,Nsamp,f1s,L1sw,rF,fs,tau01,tauAll(k),tshift);
    [HmNL(:,k), fxfdpNL, hmNL(:,k), ~] = fceDPOAEinwinSS(oaeDS,Nsamp,f1s,L1sw,rF,fs,tau01,tauNL(k),tshift);
    [HmNoise(:,k), fxfdp, hmNoise(:,k), ~] = fceDPOAEinwinSS(nfloorDS,Nsamp,f1s,L1sw,rF,fs,tau01,tauAll(k),tshift);
    [HmNoiseNL(:,k), fxfdpNL, hmNoiseNL(:,k), ~] = fceDPOAEinwinSS(nfloorDS,Nsamp,f1s,L1sw,rF,fs,tau01,tauNL(k),tshift);
    % make a matrix with NaNs in the respective parts of the responses, i.e. for specific rows in Hm
    % the matrix will then be used to combine Hm and HmNL such that we
    % use concerete roex windows for concrete frequency ranges. This
    % method allows for variable roex window width, but avoid
    % transients at the band edges if the response was processed in
    % time windows of a specific size
    if k==1  % first time window
        matrixNaN = ones(length(fxfdp),length(fdpc));
        idx1 = 1;
        idx2 = find(fxfdp>=fdpc(k+1),1,'first');
    elseif k==length(fdpc) % last time window
        idx1 = find(fxfdp>=fdpc(k-1),1,'first');
        idx2 = length(fxfdp);
    else % the rest
        idx1 = find(fxfdp>=fdpc(k-1),1,'first');
        idx2 = find(fxfdp>=fdpc(k+1),1,'first');
    end
    matrixNaN(1:idx1,k) = NaN;
    matrixNaN(idx2:end,k) = NaN;
end

dpgramNL = nanmean((HmNL.*matrixNaN).');  % NL component
dpgramAll = nanmean((Hm.*matrixNaN).');  % overall DPOAE signal
noisefNL = nanmean((HmNoiseNL.*matrixNaN).');
noisefAll = nanmean((HmNoise.*matrixNaN).');

[DPOAEcalib,fxI] = SpectrumToPascals(dpgramAll,fxfdp,GainMicA);
[DPOAEcalibNL,fxI] = SpectrumToPascals(dpgramNL,fxfdp,GainMicA);
DPOAEcalibCR = DPOAEcalib - DPOAEcalibNL; % rest, LL component

[NFLOORcalib,fxI] = SpectrumToPascals(noisefAll,fxfdp,GainMicA);
[NFLOORcalibNL,fxI] = SpectrumToPascals(noisefNL,fxfdp,GainMicA);