


% example how to run boxmodel for two tone swept sine input yielding swept
% sine DPOAEs

gain = 1.05; % parameter affecting gain in the model
F2min = 1e3; % lowest f2 frequency in Hz
F2max = 12e3; % highest f2 frequency in Hz
L1 = 60; % L1 level in dB
L2 = 50;  % L2 level in dB
f2f1 = 1.2; % f2/f1 ratio
rough = 19; % 19 yields a model with roughness, 21 yields model without roughness
Rscale = 0.05; % value which sets an extent of roughness


mainJASA20DPgramSweepRun(gain,F2min,F2max,L1,L2,f2f1,rough,Rscale)