% simulated DPOAEs
% the variant of SSS which allows for primary and secondary source
% extraction


close all;
% clear all;


addpath('fce_synchsweptsine/');
addpath('fce_lsf/');


build = 1; % 0 - only plots graph, no computations (for graph design)

if build 
L1vect = 30:10:70;
L1vect = 60;


%% model with inhomogeneities
for k = 1:length(L1vect)

    % with irregularities
    load(['Simulations/SWDPgrams/DPgramSweep_L1_' num2str(L1vect(k)) 'dB_L2_50dB_F2min1000Hz_F2max12000f2f1_120_gain105R19RS5.mat'])
    
    fs = 600e3;
    fsD = 44.1e3;
    
    f1s = 1000/rF; % get F1 frequencies
    f1e = 12000/rF;
    
    oaeDSr = resample(oae,fsD,fs); % resample to 44.1kHz
    
    load ClanekAddNoise.mat
    noise10 = 1e2*10^(10/20)*addnoise; % noise of various levels
    oaeDSr = oaeDSr + 0*noise10;
    % without irregularities, smooth
    load(['Simulations/SWDPgrams/DPgramSweep_L1_' num2str(L1vect(k)) 'dB_L2_50dB_F2min1000Hz_F2max12000f2f1_120_gain105R21RS0.mat'])
    
    oaeDSs = resample(oae,fsD,fs); % resample to 44.1kHz (from 600 kHz)
        
    
    %Twin = length(oaeDSr)*1/fsD; % window duration in seconds6
    Twin = 1;
    
    Nwin = round(Twin*fsD); % window duration in samples
    T = length(oaeDSr)*1/fsD;
%     Twin = length(oaeDS)*1/fsD; % window duration in seconds6
    Nsamp = length(oaeDSr);  % number of samples in the model response (overall time signal)
    finst = inst_freq_synch_swept_sine(f1s,f1e,T,fsD); % instantaneous freq of swept sine
    tx = 0:1/fsD:(Nsamp-1)/fsD;
    
    twA = 30e-3; % overall window for NL and CR components
    twNL = 2*2.28e-3; % NL window
    twCR = 20e-3; % CR window
    twShift = 12e-3; % shift for CR window
    Nstep = Nwin/2; % half window width step size
    
    
    
    T = Nsamp*1/fsD; % duration of the signal
    L1sw = T/log(f1e/f1s); % overall L
%     for nwin = 1:floor(Nsamp/Nwin) % loop over adjucent windows
    
    % use SSS to extract DPOAE
    finst = inst_freq_synch_swept_sine(f1s,f1e,T,fsD); % instantaneous freq of swept sine
    fc = finst(Nwin/2:Nwin:end);  % center frequencies of windows (f1 frequencies)
    fdpc = 2*fc - rF*fc;  % fdp frequency at the center frequencies
    aW = 0.01;
    tauNL = 0.5*aW*(fdpc/1000).^(-0.8);  % NL window width as a function of fdp
    tauAll = 5/2*aW*(fdpc/1000).^(-0.8); % overall window width (NL + CR components) as a function of fdp
    
%     aW = 0.01;
% tauNL = 0.5*aW*(fdpc/1000).^(-0.8);  % NL window width as a function of fdp
% tauAll = 5/2*aW*(fdpc/1000).^(-0.8); % overall window width (NL + CR components) as a function of fdp
    
    for n=1:length(fdpc)
        [HmAllr(:,n), fxAllr, hmAllr(:,n), ~,wAll(:,n)] = fceDPOAEinwinSS(oaeDSr,Nsamp,f1s,L1sw,rF,fsD,1e-3,tauAll(n),0,0);
        [HmNLr(:,n), fxNLr, hmAllNLr(:,n), ~,wNL(:,n)] = fceDPOAEinwinSS(oaeDSr,Nsamp,f1s,L1sw,rF,fsD,1e-3,tauNL(n),0,0);
        [HmAlls(:,n), fxAlls, hmAlls(:,n), ~,~] = fceDPOAEinwinSS(oaeDSs,Nsamp,f1s,L1sw,rF,fsD,1e-3,tauAll(n),0,0);
        [HmNLs(:,n), fxNLs, hmAllNLs(:,n), ~,~] = fceDPOAEinwinSS(oaeDSs,Nsamp,f1s,L1sw,rF,fsD,1e-3,tauNL(n),0,0);
        if n==1
            matrixNaN = ones(length(fxAllr),length(fdpc));
            idx1 = 1;
            idx2 = find(fxAllr>=fdpc(n+1),1,'first');
        elseif n==length(fdpc)
            idx1 = find(fxAllr>=fdpc(n-1),1,'first');
            idx2 = length(fxAllr);
        else
            idx1 = find(fxAllr>=fdpc(n-1),1,'first');
            idx2 = find(fxAllr>=fdpc(n+1),1,'first');
        end    
        matrixNaN(1:idx1,n) = NaN;
        matrixNaN(idx2:end,n) = NaN;
    end
    
    dpgramNL = nanmean((HmNLr.*matrixNaN).');  % NL component
    dpgramAll = nanmean((HmAllr.*matrixNaN).'); % overall DPOAE signal
    
    dpgramCR = dpgramAll-dpgramNL;  % estimation of the LL component (all but NL component)
        
       
%% load steady state simulations

%     oaeSW(:,k) = Hm;
%     fxdpSW(:,k) = fx;
%     oaeSW2(:,k) = Hm2;
%     fxdpSW2(:,k) = fx;
%     
end

% for k=1:length(L1vect)
%     load(['Simulations/DPss/DP_L1_' num2str(L1vect(k)) 'dB_L2_50dB_f2f11.2gain105R19rs_5.mat']);
% 
%     %oaedpSS(:,k) = oaedp(1:2:end).'.*exp(-sqrt(-1)*2*pi*Fdpvect(1:2:end).'/fs);
%     %fxdpSS(:,k) = Fdpvect(1:2:end).';
%     oaedpSS(:,k) = oaedp.'.*exp(-sqrt(-1)*2*pi*Fdpvect.'/fs);
%     fxdpSS(:,k) = Fdpvect.';
% end


% steady state DPOAEs

for k=1:length(L1vect)
    load(['Simulations/DPss/DP_L1_' num2str(L1vect(k)) 'dB_L2_50dB_f2f11.2gain105R19rs_5.mat']);

%     oaedpSS(:,k) = oaedp(1:2:end).'.*exp(-sqrt(-1)*2*pi*Fdpvect(1:2:end).'/fs);
%     fxdpSS(:,k) = Fdpvect(1:2:end).';
    oaedpSSr(:,k) = oaedp(1:end).'.*exp(-sqrt(-1)*2*pi*Fdpvect(1:end).'/fs);
    fxdpSSr(:,k) = Fdpvect(1:end).';
    
    load(['Simulations/DPss/DP_L1_' num2str(L1vect(k)) 'dB_L2_50dB_f2f11.2gain105R21rs_0.mat']);
    
    oaedpSSs(:,k) = oaedp(1:end).'.*exp(-sqrt(-1)*2*pi*Fdpvect(1:end).'/fs);
    fxdpSSs(:,k) = Fdpvect(1:end).';
end

end
% 
% 
% 
% figure;
% hold on;
% plot(fxdpSW,20*log10(abs(oaeSW)));
% plot(fxdpSS,20*log10(abs(oaedpSS)),'.');
% hold off;
% 
% xlim([670 8e3]);
% figure;
% 
% hold on;
% plot(fx,unwrap(angle(oaeSW)-2*pi*fxdpSW*len_IR/2/fsD)/(2*pi))
% plot(fxdpSS,unwrap(angle(oaedpSS))/(2*pi),'.');
% hold off;
% xlim([670 8e3]);
% 

% end

%% plot figure to the paper //..
% 
% figure;
% 
% set(gcf, 'Units', 'centimeters');
% % we set the position and dimension of the figure ON THE SCREEN
% %
% % NOTE: measurement units refer to the previous settings!
% afFigurePosition = [1 1 20 9];
% % [pos_x pos_y width_x width_y]
% set(gcf, 'Position', afFigurePosition); % [left bottom width height]
% % we link the dimension of the figure ON THE PAPER in such a way that
% % it is equal to the dimension on the screen
% %
% % ATTENTION: if PaperPositionMode is not ’auto’ the saved file
% % could have different dimensions from the one shown on the screen!
% set(gcf, 'PaperPositionMode', 'auto');
% 
% iFontSize = 12;
% strFontName = 'Helvetica';
% LWtick = 1.3;
% LW = 1.3;
% MS = 8;
% leftCornerX = 0.1;
% leftCornerY = 0.16;
% wEnv = 0.8;
% hEnv = 0.78;
% Xlims = [20 75];
% Ylims = [30 75];
% Clims = [0 800];
% Clims = [0 60];
% 
% haAbs = axes('position',[leftCornerX leftCornerY  wEnv hEnv],'xscale','log','yscale','lin','nextplot','add');
% 
% % plot(fx,abs(Ysp));
% %plot(fx,abs(HmAll));
% l1 = plot(fxAllr/1e3,20*log10(abs(dpgramAll)),'linewidth',LW,'color','k','linestyle','-');
% l2 = plot(fxAllr/1e3,20*log10(abs(dpgramNL)),'linewidth',LW,'color','k','linestyle','--');
% l3 = plot(fxAllr/1e3,20*log10(abs(dpgramCR)),'linewidth',LW,'color',[.6 .6 .6]);
% l4 = plot(fxdpSSr/1e3,20*log10(abs(oaedpSSr)),'r.');
% l5 = plot(fxdpSSr(1:4:end)/1e3,20*log10(abs(oaedpSSs(1:4:end))),'color',[0 1 0],'linestyle','none','marker','.');
% l6 = plot(fxdpSSr/1e3,20*log10(abs(oaedpSSr-oaedpSSs)),'color',[0 0 1],'linestyle','none','marker','.');
% 
% set(haAbs,...
%     'xlim',[700 8e3]/1000,...
%     'ylim',[10 55],...
%     'box','on',...
%     'xtick',[1 2 5 10],...
%     'fontsize',iFontSize,...
%     'fontname',strFontName...
%    );
% 
% text(0.55,43,'DPOAE level (dB re 1 a.u.)','fontsize',iFontSize+1,'fontname',strFontName,'rotation',90,'horizontalalignment','center')
% 
% % legend([l1(1),l2(2),l3],{'Odd wins','Even wins','Overall signal'}, 'position',[0.12 0.19 0.25 0.2]);
% xlabel('Frequency {\itf}_{\rmDP} (kHz)','fontsize',iFontSize+1,'fontname',strFontName)
% 
% 
% strFileName = ['Figures/DPOAESimWindow'];
% print('-depsc', strcat(strFileName, '.eps'));
% 
% 


parW.nmm=25;
parW.nb=6; % n. 1/3 oct bands
parW.tt=13; % latency power law: tt*f(kHz)^-bbb
parW.bbb=1;
parW.p0=1;
parW.tc=-50;
parW.tch=50;
parW.c1=-0.3;
parW.c2=0.3;
parW.c3=1;
parW.c4=2.5;
parW.c5=3;


plotflag = 1;

%[fx, DPgAlla, DPgNLaWLT, DPgCRa, DPgCR1a, DPgAllreca, DPgAllph, DPgNLph, DPgCRph, DPgCR1ph, DPgAllrecph, ...
%    fc, DPtoctAlla, DPtoctNLa, DPtoctCRa, wlgraph] = WLTdecompFCE(parW,oae.f,oaelsf,plotflag);


load FreqAxSweep.mat FF
FF2 = min(fdpc):min(diff(FF)):max(fdpc);
dpoaeTondaInt = interp1(fxAllr,dpgramAll,FF2,'linear');
% dpoaeTondaInt = interp1(fdptond,dpoaeTonda,oae.f,'spline');
% [fxWLT, DPgAlla, DPgNLa, DPgCRa, DPgCR1a, DPgAllreca, DPgAllph, DPgNLph, DPgCRph, DPgCR1ph, DPgAllrecph, ...
%     fc, DPtoctAlla, DPtoctNLa, DPtoctCRa, wlgraph] = WLTdecompFCE(parW,FF2,dpoaeTondaInt,plotflag);

figure;

set(gcf, 'Units', 'centimeters');
% we set the position and dimension of the figure ON THE SCREEN
%
% NOTE: measurement units refer to the previous settings!
afFigurePosition = [1 1 20 15];
% [pos_x pos_y width_x width_y]
set(gcf, 'Position', afFigurePosition); % [left bottom width height]
% we link the dimension of the figure ON THE PAPER in such a way that
% it is equal to the dimension on the screen
%
% ATTENTION: if PaperPositionMode is not ’auto’ the saved file
% could have different dimensions from the one shown on the screen!
set(gcf, 'PaperPositionMode', 'auto');

iFontSize = 12;
strFontName = 'Helvetica';
LWtick = 1.3;
LW = 1.3;
MS = 10;
leftCornerX = 0.1;
leftCornerY = 0.53;
wEnv = 0.8;
hEnv = 0.43;
Xlims = [20 75];
Ylims = [30 75];
Clims = [0 800];
Clims = [0 60];

haAbs = axes('position',[leftCornerX leftCornerY  wEnv hEnv],'xscale','log','yscale','lin','nextplot','add');


% plot(fx,abs(Ysp));
%plot(fx,abs(HmAll));
l1 = plot(fxAllr/1e3,20*log10(abs(dpgramAll)),'linewidth',LW,'color','k','linestyle','-');
l2 = plot(fxAllr/1e3,20*log10(abs(dpgramNL)),'linewidth',LW,'color','k','linestyle','--');
l3 = plot(fxAllr/1e3,20*log10(abs(dpgramCR)),'linewidth',LW,'color',[.6 .6 .6]);
% l7 = plot(fxWLT/1e3,DPgCRa,'linewidth',LW,'color',[.6 .6 .6]);
l4 = plot(fxdpSSr/1e3,20*log10(abs(oaedpSSr)),'r.');
l5 = plot(fxdpSSr(1:4:end)/1e3,20*log10(abs(oaedpSSs(1:4:end))),'color',[0 1 0],'linestyle','none','marker','.');
l6 = plot(fxdpSSr/1e3,20*log10(abs(oaedpSSr-oaedpSSs)),'color',[0 0 1],'linestyle','none','marker','.');

set(haAbs,...
    'xlim',[700 8e3]/1000,...
    'ylim',[10 55],...
    'xticklabel','',...
    'box','on',...
    'fontsize',iFontSize,...
    'fontname',strFontName...
   );

text(0.55,32,'DPOAE level (dB re 1 a.u.)','fontsize',iFontSize+1,'fontname',strFontName,'rotation',90,'horizontalalignment','center')

% legend('Swept-tone DPOAE, {\itN} = 10','Swept-tone DPOAE, {\itN} = 100','Discrete-tone DPOAE', 'position',[0.16 0.565 0.25 0.07]);
% legend('Swept-tone DP-gram (SSS technique)','Discrete-tone DP-gram', 'position',[0.16 0.565 0.3 0.07]);

haPh = axes('position',[leftCornerX leftCornerY-hEnv  wEnv hEnv],'xscale','log','yscale','lin','nextplot','add');

cycle = 2*pi;
angleSSs = unwrap(angle(oaedpSSs))/cycle;
angleSSr = unwrap(angle(oaedpSSr))/cycle;
angleSSb = unwrap(angle(oaedpSSr-oaedpSSs))/cycle;
idxSSstart = find(fxdpSSr>670,1,'first');

l1 = plot(fxAllr/1e3,unwrap(angle(dpgramAll))/cycle,'linewidth',LW,'color','k','linestyle','-');
l2 = plot(fxAllr/1e3,unwrap(angle(dpgramNL))/cycle,'linewidth',LW,'color','k','linestyle','--');
l3 = plot(fxAllr/1e3,unwrap(angle(dpgramCR))/cycle+2,'linewidth',LW,'color',[.6 .6 .6]);
% l7 = plot(fxWLT/1e3,unwrap(DPgCRph)/cycle-6,'linewidth',LW,'color',[.6 .6 .6]);
l4 = plot(fxdpSSr(idxSSstart:4:end)/1e3,angleSSr(idxSSstart:4:end),'r.');
l5 = plot(fxdpSSr(idxSSstart+2:4:end)/1e3,angleSSs(idxSSstart+2:4:end),'color',[0 1 0],'linestyle','none','marker','.');
l6 = plot(fxdpSSr(idxSSstart:3:end)/1e3,angleSSb(idxSSstart:3:end)-7,'color',[0 0 1],'linestyle','none','marker','.');

set(haPh,...
    'xlim',[700 8e3]/1000,...
    'ylim',[-35 4],...
    'xtick',[1 2 5 10],...
    'box','on',...
    'fontsize',iFontSize,...
    'fontname',strFontName...
   );

xlabel('Frequency {\itf}_{\rmDP} (kHz)','fontsize',iFontSize+1,'fontname',strFontName)

legend([l1,l2,l3,l4,l5,l6],{'SSS tech., DPOAE','SSS tech. NL comp.','SSS tech. CR comp.','Steady-state, DPOAE', 'Steady-state, NL comp.','Steady-state, CR comp.'}, 'position',[0.115 0.12 0.32 0.2]);

text(0.550,-15,'DPOAE phase (cycles)','fontsize',iFontSize+1,'fontname',strFontName,'rotation',90,'horizontalalignment','center')
%
strFileName = ['Figures/Figure04'];
exportgraphics(gcf, strcat(strFileName, '.eps'),'ContentType','vector');


len_IR = 2^13;

figure;
 
set(gcf, 'Units', 'centimeters');
% we set the position and dimension of the figure ON THE SCREEN
%
% NOTE: measurement units refer to the previous settings!
afFigurePosition = [1 1 10 13];
% [pos_x pos_y width_x width_y]
set(gcf, 'Position', afFigurePosition); % [left bottom width height]
% we link the dimension of the figure ON THE PAPER in such a way that
% it is equal to the dimension on the screen
%
% ATTENTION: if PaperPositionMode is not ’auto’ the saved file
% could have different dimensions from the one shown on the screen!
set(gcf, 'PaperPositionMode', 'auto');

iFontSize = 11;
strFontName = 'Helvetica';
LWtick = 1.3;
LW = 1.2;
MS = 12;
leftCornerX = 0.19;
leftCornerY = 0.59;
wEnv = 0.76;
hEnv = 0.37;
Xlims = [20 75];
Ylims = [30 75];
Clims = [0 800];
Clims = [0 60];

haImp = axes('position',[leftCornerX leftCornerY  wEnv hEnv],'xscale','lin','yscale','lin','nextplot','add');

% tx_imp = linspace(-len_IR/fsD,len_IR/fsD,len_IR)/2; % time axis
tx_imp = linspace(-len_IR/fsD,len_IR/fsD,size(hmAllNLr,1))/2; % time axis
hm_puvodni = hmAllr(:,5);
plot(tx_imp/1e-3,wAll(:,5),'color','r')
plot(tx_imp/1e-3,wNL(:,5),'color','g')
plot(tx_imp/1e-3, hm_puvodni/max(abs(hm_puvodni)),'color','k','linewidth',LW); 
%plot(tx_imp/1e-3, hm_puvodni/max(abs(hm_puvodni)),'color','k','linestyle','--'); 

%plot(tx_imp/1e-3,w2,'color',[.7 .7 .7],'linestyle','--');


set(haImp,...
    'xlim',[-10 25],...m
    'ylim',[-1.05 1.05],...
    'box','on',...
    'fontsize',iFontSize,...
    'fontname',strFontName...
   );

text(-8,0.9,'A','fontsize',iFontSize+3,'fontweight','bold','fontname',strFontName,'rotation',0,'horizontalalignment','center')

ylabel('Normalized amplitude','fontsize',iFontSize+1,'fontname',strFontName);

%legend('Swept-tone DPOAE','Discrete-tone DPOAE', 'position',[0.64 0.55 0.2 0.05]);

xlabel('Time (ms)','fontsize',iFontSize+1,'fontname',strFontName)

yD = 0.12;
haTD = axes('position',[leftCornerX leftCornerY-hEnv-yD  wEnv hEnv],'xscale','lin','yscale','lin','nextplot','add');

plot(fdpc/1e3,1000*tauNL,'.','linewidth',LW,'markersize',MS,'color','g');
plot(fdpc/1e3,1000*tauNL,'-','linewidth',LW,'markersize',MS,'color','k');
plot(fdpc/1e3,1000*tauAll,'.','linewidth',LW,'markersize',MS,'color','r');
plot(fdpc/1e3,1000*tauAll,'-','linewidth',LW,'markersize',MS,'color','k');

xlabel('Frequency (kHz)','fontsize',iFontSize+1,'fontname',strFontName)
text(-0.6,20,'\tau_c (ms)','fontsize',iFontSize+1,'fontname',strFontName,'rotation',90,'horizontalalignment','center')

text(0.8,36,'B','fontsize',iFontSize+3,'fontweight','bold','fontname',strFontName,'rotation',0,'horizontalalignment','center')


set(haTD,...
    'xlim',1e-3*[500 7000],...
    'ylim',[0 40],...
    'box','on',...
    'fontsize',iFontSize,...
    'fontname',strFontName...
   );


%
strFileName = ['Figures/Figure03'];
exportgraphics(gcf, strcat(strFileName, '.eps'),'ContentType','vector');


