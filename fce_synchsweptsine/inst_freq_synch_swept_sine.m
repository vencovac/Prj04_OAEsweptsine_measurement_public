function finst = inst_freq_synch_swept_sine(f1s,f1e,T,fs)

% calculate instantaneous frequency for synchronized swept sine

    [~,t,L] = synchronized_swept_sine(f1s,f1e,T,fs); % synchronized sw. s.

    finst = f1s*exp(t/L); % instantaneous frequency