function G = roexwin(N,n,fs,tc1,tc2)
% roex window



% fs = 44.1e3;
tx = 0:1/fs:(N/2-1)/fs;
% tx = 0:1/fs:0.04;

% n = 10;
% tc = 1e-3;

G1 = GetExpFilt(tx,tc1,n);

% tc2 = 20e-3;

G2 = GetExpFilt(tx,tc2,n);

G = [fliplr(G1) G2].';

% 
% figure;
% hold on;
% plot(-1*fliplr(tx),fliplr(G1));
% plot(tx,G2);
end


function G=GetExpFilt(t,tc,N)
    g0=1;
    for i=2:N
        g0=log(g0+1);
    end
    T=sqrt(g0)*t/tc;
    G=exp((T.^2));
    for i=2:N
      G=exp(G-1);
    end
   G=1./G;    
   
end