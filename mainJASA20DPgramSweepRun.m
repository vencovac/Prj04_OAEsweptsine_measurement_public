function mainJASA20DPgramSweepRun(gain,F2min,F2max,A1,A2,rFList,rough,roughScale)
%function mainJASA20DPgramSweepRun(gain,F2min,F2max,A1,A2,rFList,rough,roughScale)
%
% main script for runing a box model simulation for sycnhronized swetp-sine
% as input (two swept-sines to generate DPOAEs)
%
% input parameters:
% gain - model gain (1.05 used in the paper)
% F2min - minimal value of f2 freq (Hz)
% F2max - maximal value of f2 freq (Hz)
% A1 - Level of f1 tone (dB SPL)
% A2 - level of f2 tone (dB SPL)
% rFlist - freq ratio f2/f1
% rough - with inhomogen or not? set 21 for smooth cochlea or 19 for random
% roughness
% roughScale - standard deviation of the inhomogeneities (0.05 was used in the
% paper)


% Author: Vaclav Vencovsky
% Department of Radioelectronics, CTU in Prague
% email address: vaclav.vencovsky@gmail.com
% Website: https://mmtg.fel.cvut.cz/personal/vencovsky/
% July 2021; Last revision: 21-Jul-2021


addpath('fce_boxmodel/')

% for simulation

phi1o=0; % phases
phi2o=0;
visual = 1; % visulize the BM response and simulations

FolderRes = 'Simulations/SWDPgrams/';

rF = rFList;

octperseco = 0.5;
Rduro = 50e-3;
%roughScale = 0.1;

[oae,tim,x,oaeSt]=CochleaRGB_DP_DFT_MEtalSweep(F2min,F2max,rF,A2,A1,octperseco,gain,phi2o,phi1o,Rduro,visual,rough,roughScale);


 %save([FolderRes 'DPgram_L1_' num2str(A1) 'dB_L2_' num2str(A2) 'dB_F2min' num2str(F2min) 'Hz_F2max' num2str(F2max) 'Hz_gain' num2str(gain*100) '.mat'],...
 %          'UUrdp','oaedp','x','RRdp','RRF1','RRF2');
save([FolderRes 'DPgramSweep_L1_' num2str(A1) 'dB_L2_' num2str(A2) 'dB_F2min' num2str(F2min) 'Hz_F2max' num2str(F2max) 'f2f1_' num2str(rFList*100) '_gain' num2str(gain*100) 'R' num2str(rough) 'RS' num2str(100*roughScale) '.mat'],...
           'oae','x','oaeSt','tim','octperseco','Rduro','A1','A2','rF','phi2o','phi1o');       
   %      




rmpath('fce_boxmodel/')
