Files for manuscript on synchronized swept sines for DPOAEs

visTonda20_ModelVerifikacewithSSOkenkaNLCR.m is used to obtain Figures 03 and 04 in the manuscript

visTonda20_s013ExpTondavsLSFokenka6050.m is used to obtain Figure 05

visTonda20_s014ExpTondavsLSFokenka6050.m is used to obtain Figure 06

visTonda20_s015ExpTondavsLSFokenka5045.m is used to obtain Figure 07

visTonda20_s017ExpTondavsLSFokenka5045.m is used to obtain Figure 08
